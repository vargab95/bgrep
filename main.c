#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>

#define TRUE 1
#define FALSE 0

typedef uint8_t boolean;

void print_usage();
void process_hex_string(char *hex_string, uint8_t **binary_string, size_t *binary_length);
void hex_dump(uint8_t *binary_data, size_t len);
boolean check_for_match(int file, uint8_t *binary_data, size_t binary_length);
void dump_surrounding_bytes(int file, int before, int after, int match_length);

int main(int argc, char **argv) {
    int file;
    char *path;
    uint8_t *binary_string;
    size_t binary_length;

    if (argc < 3) {
        print_usage();
        return 1;
    }

    process_hex_string(argv[1], &binary_string, &binary_length);
#ifdef DEBUG
    printf("Input string: %s\n", argv[1]);
    printf("Binary: ");
    hex_dump(binary_string, binary_length);
#endif

    for (int i = 2; i < argc; i++) {
        path = argv[i];
        file = open(path, O_RDONLY);
        if (file < 0) {
            printf("%s cannot be opened\n", path);
            continue;
        }

        printf("%s:\n", path);
        if (check_for_match(file, binary_string, binary_length)) {
            dump_surrounding_bytes(file, 10, 10, binary_length);
        } else {
            puts("Not found");
        }

        close(file);
    }

    free(binary_string);
}

void print_usage() {
    puts("bgrep <data> <list of files>");
    putchar('\n');
    puts("data should be a hex string");
}

void process_hex_string(char *hex_string, uint8_t **binary_string, size_t *binary_length) {
    char *pos = hex_string;
    size_t hex_string_length = strlen(hex_string);
    int bin_cnt = 0;

    *binary_length = hex_string_length / 2;
    if (hex_string_length % 2 != 0) {
        ++(*binary_length);
    }

    *binary_string = (uint8_t*)calloc(*binary_length, sizeof(uint8_t));

    if (hex_string_length % 2 != 0) {
        if (sscanf(pos, "%1hhx", &(*binary_string)[bin_cnt])) {
            ++bin_cnt;
            ++pos;
        }
    }

    while(sscanf(pos, "%2hhx", &(*binary_string)[bin_cnt])) {
        ++bin_cnt;
        pos += 2;
        if (bin_cnt >= *binary_length) {
            break;
        }
    }
}

void hex_dump(uint8_t *binary_data, size_t len) {
    for (int i = 0; i < len; i++) {
        printf("%02hhx ", binary_data[i]);
    }
    putchar('\n');
}

boolean check_for_match(int file, uint8_t *binary_data, size_t binary_length) {
    int matched_count = 0;
    uint8_t byte;

    while (read(file, &byte, sizeof(byte))) {
        if (byte == binary_data[matched_count]) {
            ++matched_count;
        } else {
            if (matched_count) {
                // Jump to the next character
                lseek(file, -matched_count - 1, SEEK_SET);
                matched_count = 0;
            }
        }

        if (matched_count == binary_length) {
            return TRUE;
        }
    }

    return FALSE;
}

void dump_surrounding_bytes(int file, int before, int after, int match_length) {
    off_t current_position;
    off_t backward_offset;
    size_t no_printed_bytes;
    size_t bytes_read;
    uint8_t *bytes;

    current_position = lseek(file, 0, SEEK_CUR);

    backward_offset = before + match_length;
    if (current_position < backward_offset) {
        backward_offset = current_position;
    }

    lseek(file, -backward_offset, SEEK_CUR);

    no_printed_bytes = backward_offset + after;
    bytes = (uint8_t*)calloc(no_printed_bytes, sizeof(uint8_t));
    if (NULL == bytes) {
        puts("ERROR: Cannot allocate memory for printout");
        exit(-1);
    }

    bytes_read = read(file, bytes, no_printed_bytes);
    hex_dump(bytes, bytes_read);

    for (int i = 0; i < backward_offset * 3; ++i) {
        if ((i >= (backward_offset - match_length) * 3) && (i % 3 != 2)) {
            putchar('^');
        } else {
            putchar(' ');
        }
    }
    putchar('\n');
}
